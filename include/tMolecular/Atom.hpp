/* tMolecule: xxx
   Copyright 2020 Bin Gao

   This Source Code Form is subject to the terms of the Mozilla Public
   License, v. 2.0. If a copy of the MPL was not distributed with this
   file, You can obtain one at http://mozilla.org/MPL/2.0/.

   This file is the header file of molecular system.

   2020-04-05, Bin Gao:
   * first version
*/

#pragma once

#include <memory>

namespace tMolecular
{
    /* Class for atoms */
    template<typename RealType> class Atom
    {
        public:
            explicit Atom(const unsigned int atomicNumber,
                          const RealType atomicMass,
                          const std::array<RealType,3> coordinates) noexcept:
                m_atomic_number(atomicNumber),
                m_atomic_mass(atomicMass),
                m_coordinates(coordinates) {}
            virtual ~Atom() noexcept = default;
            /* Returns type name */
            inline std::string type_name() const noexcept { return std::string("Atom"); }
            /* Brief information */
            inline std::string to_string() const noexcept
            {
                return "{atom: atomic-number: "+std::to_string(m_atomic_number)
                    +", atomic mass: "+std::to_string(m_atomic_mass)
                    +", coordinates: ["+std::to_string(m_coordinates[0])
                    +", "+std::to_string(m_coordinates[1])
                    +", "+std::to_string(m_coordinates[2])+"]}";
            }
            /* Gets atomic mass */
            inline RealType get_atomic_mass() const noexcept
            {
                return m_atomic_mass;
            }
            /* Gets coordinates */
            inline std::array<RealType,3> get_coordinates() const noexcept
            {
                return m_coordinates;
            }
        private:
            /* Atomic number */
            unsigned int m_atomic_number;
            /* Atomic mass */
            RealType m_atomic_mass;
            /* Coordinates */
            std::array<RealType,3> m_coordinates;
            /* Velocity */
    };
}
