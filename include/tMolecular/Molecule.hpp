/* tMolecule: xxx
   Copyright 2018 Bin Gao

   This Source Code Form is subject to the terms of the Mozilla Public
   License, v. 2.0. If a copy of the MPL was not distributed with this
   file, You can obtain one at http://mozilla.org/MPL/2.0/.

   This file is the header file of molecular system.

   2018-07-05, Bin Gao:
   * first version
*/

#pragma once

#include <memory>

#include "nonstd/expected-lite/expected.hpp"
#include "tGlueCore/tBasicTypes.hpp"

namespace tMolecule
{
    /* Class for atoms */
    class tAtom
    {
        public:
            /* Returns type name */
            inline std::string type_name() const noexcept { return std::string("tAtom"); }
            /* Brief information */
            inline std::string brief() const noexcept
            {
                std::string str_brief = type_name()
                                      + "\nAtomic number="
                                      + std::to_string(m_atomic_number)
                                      + "\nAtomic mass="
                                      + std::to_string(m_atomic_mass)
                                      + "\nCoordinates={ ";
                for (auto& icoord: m_coordinates) str_brief += std::to_string(*icoord)+" ";
                str_brief += "}";
                return str_brief;
            }
            /* Gets atomic mass */
            inline tReal get_atomic_mass() const noexcept
            {
                return m_atomic_mass;
            }
            /* Gets coordinates */
            inline std::array<tReal,3> get_coordinates() const noexcept
            {
                return m_coordinates;
            }
            ~tAtom() noexcept = default;
        private:
            /* Atomic number */
            tUInt m_atomic_number;
            /* Atomic mass */
            tReal m_atomic_mass;
            /* Coordinates */
            std::array<tReal,3> m_coordinates;
            /* Velocity */
    };

    /* Class for clusters and periodic systems? */
    class tMol: public std::enable_shared_from_this<tMol>
    {
        public:
            /* Returns type name */
            inline std::string type_name() const noexcept { return std::string("tMol"); }
            /* Brief information */
            inline std::string brief() const noexcept
            {
                std::string str_brief = type_name()+"\nAtoms=";
                for (auto& iatom: m_atoms) str_brief += "\n"+iatom->brief();
                str_brief += "\nGauge origin={ ";
                for (auto& icoord: m_gauge_origin) str_brief += std::to_string(*icoord)+" ";
                str_brief += "}\nCenter of mass={ ";
                for (auto& icoord: m_center_of_mass) str_brief += std::to_string(*icoord)+" ";
                str_brief += "}";
                return str_brief;
            }
            /* Creates a molecular system instance */
            template<typename... Args>
            static nonstd::expected<std::shared_ptr<tMol>,std::string>
            create(Args&&... args) noexcept
            {
                auto molecule = std::shared_ptr<tMol>(new(std::nothrow) tMol(std::forward<Args>(args)...));
                if (molecule) {
                    return molecule;
                }
                else {
                    std::ostringstream error_message;
                    error_message << type_name()
                                  << "::create() failed to allocate!\n";
                    return nonstd::make_unexpected(error_message.str());
                }
            }
            /* Gets an instance of the molecular system */
            inline std::shared_ptr<tMol> get_ptr() noexcept
            {
                return shared_from_this();
            }
            /* Gets gauge origin */
            inline std::array<tReal,3> get_gauge_origin() const noexcept
            {
                return m_gauge_origin;
            }
            /* Gets center of mass of the molecular system */
            inline std::array<tReal,3> get_center_of_mass() const noexcept
            {
                return m_center_of_mass;
            }
            /* Gets inertia tensor of the molecular system */
            inline std::array<tReal,6> get_inertia_tensor() const noexcept
            {
                return m_inertia_tensor;
            }
            /* The molecular system aligned to its principal axes or not */
            inline bool is_principal_aligned() const noexcept
            {
                return m_principal_aligned;
            }
            /* Aligns the molecular system to its principal axes */
            void align_to_principal_axes() noexcept
            {
                if (m_principal_aligned) return;
                /* A molecule may be classified as follows according to the
                   relative values of Ia, Ib and Ic:
                   Ia=Ib=Ic: spherical top;
                   Ia=Ib<Ic: oblate symmetric top;
                   Ia<Ib=Ic: prolate symmetric top;
                   Ia<Ib<Ic: asymmetric top. */
                /*
https://en.wikipedia.org/wiki/Eigenvalue_algorithm#3%C3%973_matrices

Smith, Oliver K. (April 1961), "Eigenvalues of a symmetric 3 × 3 matrix.", Communications of the ACM, 4 (4): 168

% Given a real symmetric 3x3 matrix A, compute the eigenvalues
% Note that acos and cos operate on angles in radians

p1 = A(1,2)^2 + A(1,3)^2 + A(2,3)^2
if (p1 == 0) 
   % A is diagonal.
   eig1 = A(1,1)
   eig2 = A(2,2)
   eig3 = A(3,3)
else
   q = trace(A)/3               % trace(A) is the sum of all diagonal values
   p2 = (A(1,1) - q)^2 + (A(2,2) - q)^2 + (A(3,3) - q)^2 + 2 * p1
   p = sqrt(p2 / 6)
   B = (1 / p) * (A - q * I)    % I is the identity matrix
   r = det(B) / 2

   % In exact arithmetic for a symmetric matrix  -1 <= r <= 1
   % but computation error can leave it slightly outside this range.
   if (r <= -1) 
      phi = pi / 3
   elseif (r >= 1)
      phi = 0
   else
      phi = acos(r) / 3
   end

   % the eigenvalues satisfy eig3 <= eig2 <= eig1
   eig1 = q + 2 * p * cos(phi)
   eig3 = q + 2 * p * cos(phi + (2*pi/3))
   eig2 = 3 * q - eig1 - eig3     % since trace(A) = eig1 + eig2 + eig3
end
                 */
            }
            ~tMol() noexcept = default;
        protected:
            /* Sets information related to rigid body dynamics, such as center
               of mass and inertia tensor */
            void set_rigid_body() noexcept
            {
                for (auto& iatom: m_atoms) {
                    auto atomic_mass = iatom->get_atomic_mass();
                    m_molecular_mass += atomic_mass;
                    auto atomic_coordinates = iatom->get_coordinates();
                    std::array<tReal,3> atomic_coord_squares;
                    for (tUInt icoord=0; icoord<3; ++icoord) {
                        m_center_of_mass[icoord] += atomic_mass*atomic_coordinates[icoord];
                        atomic_coord_squares[icoord] = atomic_coordinates[icoord]
                                                     * atomic_coordinates[icoord];
                    }
                    m_inertia_tensor[0] += atomic_mass*(atomic_coord_squares[1]+atomic_coord_squares[2]);
                    m_inertia_tensor[1] += atomic_mass*(atomic_coord_squares[0]+atomic_coord_squares[2]);
                    m_inertia_tensor[2] += atomic_mass*(atomic_coord_squares[0]+atomic_coord_squares[1]);
                    m_inertia_tensor[3] += atomic_mass*atomic_coordinates[0]*atomic_coordinates[1];
                    m_inertia_tensor[4] += atomic_mass*atomic_coordinates[1]*atomic_coordinates[2];
                    m_inertia_tensor[5] += atomic_mass*atomic_coordinates[0]*atomic_coordinates[2];
                }
                for (auto& icoord: m_center_of_mass) *icoord /= m_molecular_mass;
                m_inertia_tensor[3] = -m_inertia_tensor[3];
                m_inertia_tensor[4] = -m_inertia_tensor[4];
                m_inertia_tensor[5] = -m_inertia_tensor[5];
            }
        private:
            explicit tMol(const bool& alignPrincipal=true) noexcept:
                m_principal_aligned(false),
                m_molecular_mass(0.0)
            {
                /* Sets information related to rigid body dynamics */
                set_rigid_body();
                /* Orients the molecular system so that its center of mass is
                   at the coordinate system origin, and its principal axes are
                   aligned to the coordinate axes (its inertia tensor becomes a
                   diagonal matrix) */
                if (alignPrincipal) {
                    m_principal_aligned = true;
                }
            }
            tMol(const tMol& other) noexcept = default;
            /* Whether the principal axes of the molecular system are aligned
               to the coordinate axes */
            bool m_principal_aligned;
            /* Molecular mass, as the sum of atomic masses */
            tReal m_molecular_mass;
            /* Gauge origin */
            std::array<tReal,3> m_gauge_origin;
            /* Center of mass */
            std::array<tReal,3> m_center_of_mass;
            /* Inertia tensor in the form of {Ixx, Iyy, Izz, Ixy, Iyz, Ixz} */
            std::array<tReal,6> m_inertia_tensor;
            /* Atoms */
            std::vector<tAtom> m_atoms;
    };
}
