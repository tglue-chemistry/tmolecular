Authors
=======

The main developing team of this program and previous authors (if their
contributions are still used) consist of (alphabetical order of surname):
    
.. tabularcolumns:: |p{0.1\textwidth}|p{0.4\textwidth}|p{0.1\textwidth}|p{0.4\textwidth}|
.. list-table:: QcMolecule authors
   :header-rows: 1

   * - Name
     - Affiliation
     - Email
     - Contribution
   * - Bin Gao
     - UiT The Arctic University of Norway
     - bin.gao@@uit.no
     - Manager & Developer
