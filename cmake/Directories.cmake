# Directories containing manuals, header and source code files
set(QCMOLECULE_DOC_DIR ${LIB_QCMOLECULE_PATH}/doc)
set(QCMOLECULE_HEADER_DIR ${LIB_QCMOLECULE_PATH}/include)
set(QCMOLECULE_C_DIR ${LIB_QCMOLECULE_PATH}/src)
set(QCMOLECULE_FORTRAN_DIR ${LIB_QCMOLECULE_PATH}/src/fortran)
set(QCMOLECULE_C_TEST_DIR ${LIB_QCMOLECULE_PATH}/tests)
set(QCMOLECULE_FORTRAN_TEST_DIR ${LIB_QCMOLECULE_PATH}/tests/fortran)
# For developers to build from WEB files
if(QCMOLECULE_BUILD_WEB)
    # We process LaTeX files in the build directory
    set(QCMOLECULE_LATEX_BUILD_DIR ${CMAKE_BINARY_DIR}/latex)
    # We will create these directories before generating and processing LaTeX,
    # header and source code files
    set(QCMOLECULE_MAKE_DIRS
        ${QCMOLECULE_DOC_DIR}
        ${QCMOLECULE_HEADER_DIR}
        ${QCMOLECULE_C_DIR}
        ${QCMOLECULE_FORTRAN_DIR}
        ${QCMOLECULE_C_TEST_DIR}
        ${QCMOLECULE_FORTRAN_TEST_DIR}
        ${QCMOLECULE_LATEX_BUILD_DIR})
endif()
