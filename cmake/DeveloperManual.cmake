# LaTeX master file
set(QCMOLECULE_DEVELOPER_MANUAL "QcMoleculeDeveloper")
# Weaved LaTeX chapter
set(QCMOLECULE_DEVELOPER_WEAVED_TEX
    ${LIB_QCMOLECULE_PATH}/web/ChapterWeaved.tex)
