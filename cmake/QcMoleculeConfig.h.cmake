# File name of the configure file
SET(QCMOLECULE_CONFIG ${PROJECT_BINARY_DIR}/QcMoleculeConfig.h)
# Writes all the options specified by the user
FILE(WRITE ${QCMOLECULE_CONFIG} "#if !defined(QCMOLECULE_CONFIG_H)\n")
FILE(APPEND ${QCMOLECULE_CONFIG} "#define QCMOLECULE_CONFIG_H\n\n")
FILE(APPEND ${QCMOLECULE_CONFIG} "#endif\n")
