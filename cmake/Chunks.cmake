# Chunks of the library
set(QCMOLECULE_HEADER_CHUNKS
    MolOneOper.h
    QcMolecule.h
    QcMoleculeTest.h)
set(QCMOLECULE_C_CHUNKS
    MolOneOper.c)
set(QCMOLECULE_C_TEST_CHUNKS
    MolOneOperTest.c
    QcMoleculeTest.c)
set(QCMOLECULE_FORTRAN_CHUNKS
    MolOneOperFortranAdapter.c
    MolOneOper.F90
    QcMolecule.F90)
set(QCMOLECULE_FORTRAN_TEST_CHUNKS
    MolOneOperTest.F90
    QcMoleculeTest.F90)
