# Options for making the library
option(QCMOLECULE_BUILD_WEB "Build QcMolecule from WEB files." OFF)
option(QCMOLECULE_TEST_EXECUTABLE "Build test suite as excutables." ON)
option(QCMOLECULE_FORTRAN_API "Build Fortran 2003 APIs." OFF)
