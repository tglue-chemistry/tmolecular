# WEB files
set(QCMOLECULE_WEB_FILES
    ${LIB_QCMOLECULE_PATH}/web/Implementation.nw
    ${LIB_QCMOLECULE_PATH}/web/NucHamiltonian.nw
    ${LIB_QCMOLECULE_PATH}/web/MolOverlap.nw
    ${LIB_QCMOLECULE_PATH}/web/MolOneInt.nw
    ${LIB_QCMOLECULE_PATH}/web/MolTwoInt.nw
    ${LIB_QCMOLECULE_PATH}/web/MolXCFun.nw
    ${LIB_QCMOLECULE_PATH}/web/FortranAPIs.nw
    ${LIB_QCMOLECULE_PATH}/web/UnitTesting.nw)
