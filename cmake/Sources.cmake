# Generates lists of source files from directories and chunks
#
# Header files
if(NOT (QCMOLECULE_HEADER_DIR AND QCMOLECULE_HEADER_CHUNKS))
    message(FATAL_ERROR "Directories.cmake and Chunks.cmake not included.")
endif()
set(QCMOLECULE_HEADER_FILES)
foreach(CHUNK ${QCMOLECULE_HEADER_CHUNKS})
    set(QCMOLECULE_HEADER_FILES
        ${QCMOLECULE_HEADER_FILES}
        ${QCMOLECULE_HEADER_DIR}/${CHUNK})
endforeach()
# C source codes
if(NOT (QCMOLECULE_C_DIR AND QCMOLECULE_C_CHUNKS))
    message(FATAL_ERROR "Directories.cmake and Chunks.cmake not included.")
endif()
set(QCMOLECULE_C_FILES)
foreach(CHUNK ${QCMOLECULE_C_CHUNKS})
    set(QCMOLECULE_C_FILES
        ${QCMOLECULE_C_FILES}
        ${QCMOLECULE_C_DIR}/${CHUNK})
endforeach()
# C test source codes
if(NOT (QCMOLECULE_C_TEST_DIR AND QCMOLECULE_C_TEST_CHUNKS))
    message(FATAL_ERROR "Directories.cmake and Chunks.cmake not included.")
endif()
set(QCMOLECULE_C_TEST_FILES)
foreach(CHUNK ${QCMOLECULE_C_TEST_CHUNKS})
    set(QCMOLECULE_C_TEST_FILES
        ${QCMOLECULE_C_TEST_FILES}
        ${QCMOLECULE_C_TEST_DIR}/${CHUNK})
endforeach()
# Fortran source codes
if(NOT (QCMOLECULE_FORTRAN_DIR AND QCMOLECULE_FORTRAN_CHUNKS))
    message(FATAL_ERROR "Directories.cmake and Chunks.cmake not included.")
endif()
set(QCMOLECULE_FORTRAN_FILES)
foreach(CHUNK ${QCMOLECULE_FORTRAN_CHUNKS})
    set(QCMOLECULE_FORTRAN_FILES
        ${QCMOLECULE_FORTRAN_FILES}
        ${QCMOLECULE_FORTRAN_DIR}/${CHUNK})
endforeach()
# Fortran test source codes
if(NOT (QCMOLECULE_FORTRAN_TEST_DIR AND QCMOLECULE_FORTRAN_TEST_CHUNKS))
    message(FATAL_ERROR "Directories.cmake and Chunks.cmake not included.")
endif()
set(QCMOLECULE_FORTRAN_TEST_FILES)
foreach(CHUNK ${QCMOLECULE_FORTRAN_TEST_CHUNKS})
    set(QCMOLECULE_FORTRAN_TEST_FILES
        ${QCMOLECULE_FORTRAN_TEST_FILES}
        ${QCMOLECULE_FORTRAN_TEST_DIR}/${CHUNK})
endforeach()
